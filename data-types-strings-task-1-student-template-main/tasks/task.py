def get_fractions(a_b: str, c_b: str) -> str:
    a, b = map(int, a_b.split('/'))
    c, _ = map(int, c_b.split('/'))
    d = b
    e = a + c
    return f"{a_b} + {c_b} = {e}/{d}"

print(get_fractions('1/3', '5/3')) # Output: '1/3 + 5/3 = 6/3'